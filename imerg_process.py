
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from scipy.ndimage.filters import gaussian_filter
from os import listdir , walk
import os
import h5py as h5
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib
import imageio
import netCDF4 as nc 
import datetime
import plotly.express as px
from sklearn.metrics import mean_squared_error, r2_score,mean_absolute_error
import statsmodels.api as sm
import pylab as py 
import scipy.stats as stats



def get_lon_lat_observ(name):
    "get the latitude and longitude of the station form metadata"
    """name could be         ODIENNE
                              KORHOGO
                       FERKESSEDOUGOU
                       BONDOUKOU/SOKO
                                  MAN
                               BOUAKE
                               GAGNOA
                                DALOA
                        DIMBOKRO CITY
                         YAMOUSSOUKRO
  ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                               ADIAKE
                                TABOU
                            SAN PEDRO
                      SASSANDRA/DREWIN"""
    observ_history=pd.read_csv('/home/jovyan/data/observations/gsod/isd-history.csv')
    lat= observ_history.loc[observ_history['STATION NAME']==name].iloc[0][6]
    lon= observ_history.loc[observ_history['STATION NAME']==name].iloc[0][7]
    return lon,lat

def get_observ(name):
    """get observation data for the station specified by name
    
     name could be         ODIENNE
                              KORHOGO
                       FERKESSEDOUGOU
                       BONDOUKOU/SOKO
                                  MAN
                               BOUAKE
                               GAGNOA
                                DALOA
                        DIMBOKRO CITY
                         YAMOUSSOUKRO
  ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                               ADIAKE
                                TABOU
                            SAN PEDRO
                      SASSANDRA/DREWIN"""
    
    metadata=pd.read_pickle('/home/jovyan/data/observations/gsod/station_metadata.pkl')
    CIV_data = metadata.loc[metadata["ctry"]=='IV'].astype({'id': 'int64'})
    observations=pd.read_pickle('/home/jovyan/data/observations/gsod/gsod_2019.pkl')
    observ=observations.astype({'id': 'int64'})
    ide = CIV_data.loc[CIV_data['station name']==name].iloc[0][0]
    
    observ_name=observ.loc[observ['id']==ide]
    observ_precp_abidj= observ_name[['date','prcp']]

    observ_precp=observ_precp_abidj.set_index('date')

    observ_precp=observ_precp.to_xarray()
    return observ_precp

def combine_observ_pred(pred,observ):
    
    result = pd.concat([pred.to_dataframe(), observ.to_dataframe().fillna(method="ffill")], axis=1, sort=False)
    
    result=result[['precipitationCal','prcp']]
    result=result.rename(columns={"precipitationCal":"imerg_precp","prcp": "observed precp"})
    return result


def compute_errors(d):
    y_pred = d.imerg_precp
    y_true = d['observed precp']
    MSE = mean_squared_error(y_true, y_pred)
    MAE = mean_absolute_error(y_pred,y_true)

    Variance = np.var(y_pred)
    SSE = np.mean((np.mean(y_pred) -y_true)** 2) # Where Y is your dependent variable. # SSE : Sum of squared errors.

    Bias = (SSE - Variance)/Variance
    return MAE, MSE , Bias


def slice_data_to_seasons(data):
    
    
    """Slicing the dataset to get four different seasons
    param: data the dataset to slice
    return: a list of datasets correspending to four different seasons"""

    slice_data1 = data.sel(date=slice('2019-01', '2019-02'))
    slice_data2 = data.sel(date=slice('2019-03', '2019-05'))
    slice_data3 = data.sel(date=slice('2019-06', '2019-08'))
    slice_data4 = data.sel(date=slice('2019-09', '2019-12'))
    slices=[slice_data1,
    slice_data2, 
    slice_data3, 
    slice_data4 ]
    return slices

def plot_qq_seasons(data):
    """plot quantile to quantile data for four seasons"""
    slices=slice_data_to_seasons(data)
    

    fig = plt.figure(figsize=(10,10))
    i=1
    titles=['jan-feb','march-may','june-aug','sept-dec']
    for sl in slices:

        ax = fig.add_subplot(2, 2, i)
        mod_fit = sm.OLS(sl.to_dataframe().imerg_precp,sl.to_dataframe()['observed precp']).fit()
        res = mod_fit.resid # residuals
        sm.qqplot(res,line='45',fit=True,ax=ax)
        plt.xlabel("Imerg prediction")
        plt.ylabel("Observation")
        plt.title(titles[i-1])

        i+=1
        
def plot_qq(data):
    """ plot quantile to quantile data """
    mod_fit = sm.OLS(data.imerg_precp,data['observed precp']).fit()
    res = mod_fit.resid # residuals
    fig = sm.qqplot(res,line='45',fit=True)
    plt.xlabel("Imerg prediction")
    plt.ylabel("Observation")
    py.show()
    
def get_observ_pred(station_name,year):
    
    
    
    """get the obseration and prediction data in one dataset
       get_observ_pred(station_name,year)
           name could be         ODIENNE
                              KORHOGO
                       FERKESSEDOUGOU
                       BONDOUKOU/SOKO
                                  MAN
                               BOUAKE
                               GAGNOA
                                DALOA
                        DIMBOKRO CITY
                         YAMOUSSOUKRO
                          ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                               ADIAKE
                                TABOU
                            SAN PEDRO
                      SASSANDRA/DREWIN"""
        
    my_data=xr.open_mfdataset('./IMERG/precipitation_date/'+year+'/*.nc',combine='by_coords')
    observ_fer = get_observ(station_name)
    fer_lon,fer_lat=get_lon_lat_observ(station_name)
    imerg_fer=my_data.sel(lon=fer_lon,lat=fer_lat,method='nearest')
    fer = combine_observ_pred(observ_fer,imerg_fer)
    return fer      
